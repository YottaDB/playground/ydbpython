from yottadb import *

def main():
    ctx = Context()
    ctx.key('^words').delete()
    ctx.key('^index').delete()
    try:
        with open('wordfreq_input.txt') as f:
            words = ctx.key('^words')
            for line in f.readlines():
                for word in line.split(" "):
                    word = word.strip().lower()
                    if word != "":
                        words[word].incr()

            # Iterate through words and store based on frequency
            index = ctx.key("^index")
            for k in words:
                index[words[k.val()].get()][k.val()].set("")

            # Print the keys ordered by amount
            for k in reversed(index):
                for k2 in ctx.key("^index", k.val()):
                    print("%s\t%s" % (k.val().decode('ascii'), k2.val().decode('ascii')))
    except IOError:
        print("Failed to open file")

if __name__ == "__main__":
    main()
