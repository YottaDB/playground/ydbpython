# Copyright (c) 2019 YottaDB LLC and/or its subsidiaries.
# All rights reserved.
#
#	This source code contains the intellectual property
#	of its copyright holder(s), and is made available
#	under a license.  If you do not know the terms of
#	the license, please stop and do not read further.
import os
from cffi import FFI
ffibuilder = FFI()

# cdef() expects a single string declaring the C types, functions and
# globals needed to use the shared object. It must be in valid C syntax.
ffibuilder.cdef("""
typedef struct
{
	unsigned int	len_alloc;
	unsigned int	len_used;
	char		*buf_addr;
} ydb_buffer_t;


typedef	int (*ydb_tp2fnptr_t)(uint64_t tptoken, ydb_buffer_t *errstr, void *tpfnparm); /* For use in SimpleThreadAPI */

int	ydb_data_st(uint64_t tptoken, ydb_buffer_t *errstr, ydb_buffer_t *varname, int subs_used, ydb_buffer_t *subsarray,
			unsigned int *ret_value);
int	ydb_delete_st(uint64_t tptoken, ydb_buffer_t *errstr, ydb_buffer_t *varname, int subs_used, ydb_buffer_t *subsarray,
			int deltype);
int	ydb_delete_excl_st(uint64_t tptoken, ydb_buffer_t *errstr, int namecount, ydb_buffer_t *varnames);
int	ydb_get_st(uint64_t tptoken, ydb_buffer_t *errstr, ydb_buffer_t *varname, int subs_used, ydb_buffer_t *subsarray,
			ydb_buffer_t *ret_value);
int	ydb_incr_st(uint64_t tptoken, ydb_buffer_t *errstr, ydb_buffer_t *varname, int subs_used, ydb_buffer_t *subsarray,
			ydb_buffer_t *increment, ydb_buffer_t *ret_value);
int	ydb_lock_st(uint64_t tptoken, ydb_buffer_t *errstr, unsigned long long timeout_nsec, int namecount, ...);
	/* ... above translates to one or more sets of [ydb_buffer_t *varname, int subs_used, ydb_buffer_t *subsarray] */
int	ydb_lock_decr_st(uint64_t tptoken, ydb_buffer_t *errstr, ydb_buffer_t *varname, int subs_used, ydb_buffer_t *subsarray);
int	ydb_lock_incr_st(uint64_t tptoken, ydb_buffer_t *errstr, unsigned long long timeout_nsec, ydb_buffer_t *varname,
			int subs_used, ydb_buffer_t *subsarray);
int	ydb_node_next_st(uint64_t tptoken, ydb_buffer_t *errstr, ydb_buffer_t *varname, int subs_used, ydb_buffer_t *subsarray,
			int *ret_subs_used, ydb_buffer_t *ret_subsarray);
int	ydb_node_previous_st(uint64_t tptoken, ydb_buffer_t *errstr, ydb_buffer_t *varname, int subs_used, ydb_buffer_t *subsarray,
			int *ret_subs_used, ydb_buffer_t *ret_subsarray);
int	ydb_set_st(uint64_t tptoken, ydb_buffer_t *errstr, ydb_buffer_t *varname, int subs_used, ydb_buffer_t *subsarray,
			ydb_buffer_t *value);
int	ydb_str2zwr_st(uint64_t tptoken, ydb_buffer_t *errstr, ydb_buffer_t *str, ydb_buffer_t *zwr);
int	ydb_subscript_next_st(uint64_t tptoken, ydb_buffer_t *errstr, ydb_buffer_t *varname, int subs_used,
			ydb_buffer_t *subsarray, ydb_buffer_t *ret_value);
int	ydb_subscript_previous_st(uint64_t tptoken, ydb_buffer_t *errstr, ydb_buffer_t *varname, int subs_used,
			ydb_buffer_t *subsarray, ydb_buffer_t *ret_value);
int	ydb_tp_st(uint64_t tptoken, ydb_buffer_t *errstr, ydb_tp2fnptr_t tpfn, void *tpfnparm, const char *transid, int namecount,
			ydb_buffer_t *varnames);
int	ydb_zwr2str_st(uint64_t tptoken, ydb_buffer_t *errstr, ydb_buffer_t *zwr, ydb_buffer_t *str);

extern "Python" int tpCallback(uint64_t tptoken, ydb_buffer_t *errstr, void *tpfnparm);
""")

ydb_dir=os.environ.get('ydb_dist')

# set_source() gives the name of the python extension module to
# produce, and some C source code as a string.  This C code needs
# to make the declarated functions, types and globals available,
# so it is often just the "#include".
ffibuilder.set_source("_yottadb",
"""
     #include <libyottadb.h>   // the C header of the library
""",
     libraries=['yottadb'],
     include_dirs=[ydb_dir],
     library_dirs=[ydb_dir]
     )

if __name__ == "__main__":
    ffibuilder.compile(verbose=True)
